/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */




#include <memory>
#include <basics/Scene>
#include <basics/Texture_2D>
#include <basics/Texture_2D>
#include <basics/Timer>

#include "Board.hpp"
#include "Boton.hpp"
#pragma once
#include "MainMenu.hpp"


//using example::Boton;


using
basics::Texture_2D;

namespace example
{
#pragma once
    /**
     * Clase Simon
     */
    class Simon : public basics::Scene
    {
        /**
         *  Estado del juego
         */
        enum Mode
        {
            Player,Sequence,End,Paused,Start
        };

        /**
         * Dificultad seleccionada
         */
        enum Difficulty
        {
            EASY,MEDIUM,HARD
        };

        typedef std::shared_ptr< basics::Texture_2D > Texture_Handle;


    public:

        enum State
        {
            LOADING,
            RUNNING,
        };
        Mode           mode;
        State          state;
        Difficulty     difficulty;
        bool           suspended;

        unsigned       canvas_width;
        unsigned       canvas_height;

        /**
         *  Botones del juego
         */
        Boton Win, Lose, Restart, Exit, Pause, Easy, Medium, Hard,Information;

        /**
         * Tiempo entre elementos en la secuencia
         */
        float interval;

        /**
         * total de turnos del juego
         */
        u_long totalTurnos;
        float          x, y;
        /**
         * Tablero
         */
        Board board;
        /**
         * Vectores de la secuencia
         */
        std::vector <int>  sequence;
        /**
         * Vectores del jugador
         */
        std::vector<int> player;

        /**
         * Turno en el que nos encontramos
         */
        int  turno;

        /**
         * usaremos esta variable para recorrer y rellenar el array del jugador hasta el valor de turnos
         */
        int  turnoPos;
        /**
         * Timer
         */
        basics::Timer timer;
        /**
         * Tiempo transcurrido
         */
        float elapsedTime;

        /**
         * Indicador de victoria
         */
        bool win;
    public:

        /**
         * Constructor
         */
        Simon();

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        /**
         * Función que se ejecuta al inicar la clase
         * @return
         */
        bool initialize () override;
        /**
         * Función que se ejecuta cuando el juego está en suspensión
         */
        void suspend    () override;//Cuando el juego se pone en suspension
        /**
         * Función que se ejecuta cuando se reanuda el juego
         */
        void resume     () override;//Cuando el juego se renauda

        /**
         * Controla los eventos de pantalla
         *
         * @param event Evento de pantalla
         */
        void handle     (basics::Event & event) override; //controla los eventos de pantalla

        /**
         * En el se incluye la lógica del juego
         * @param time
         */
        void update     (float time) override;
        /**
         * Se encarga de mostrar por pantalla todos lo sprites y limpiar el canvas
         * @param context Contexto de la app
         */
        void render     (basics::Graphics_Context::Accessor & context) override;

    private:

        /**
         * Carga los datos (ene este caso es las texturas)
         */
        void load ();

        /**
         * se encarga de llamar a todas las funciones necesarias por frame
         * @param time
         */
        void run  (float time);

        /**
         * Pone a punto todas las variable para terminar el juego
         */
        void GameOver(bool state);
        /**
         * Crea la secuencia de botones
         */
        void StartSequence();
        /**
         * Damos tamaño a los botones
         */
        void SetMeasureButtons();
        /**
         * Damos posición a los botones
         */
        void SetPositionButtons();

        /**
         * Se comprueba si el jugador ha acertado la secuencia
         * @return booleano que incdica el resultado
         */
        bool CheckCorrect();
        /**
         * Selecciona la dificultad
         */
        void SelectDificulty();

    };

}
