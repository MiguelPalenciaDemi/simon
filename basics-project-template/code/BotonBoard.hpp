/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <basics/Scene>
#include <basics/Texture_2D>
#include <basics/Texture_2D>

using basics::Texture_2D;

namespace example
{

    /**
     * Clase BotonBoard, los botones del tablero
     */
    class BotonBoard
    {

        typedef std::shared_ptr< basics::Texture_2D > Texture_Handle;
    public:
        /**
         * Valores posibles
         */
        enum Id
        {
            NONE=4,A=0,B=1,C=2,D=3
        };

        /**
         *Sprite
         */
        Texture_Handle image;
        /**
         * Dimension
         */
        float width,height;
        /**
         * Posicion
         */
        float x,y;//Posicion

        /**
         * Valor
         */
        Id value;

        /**
         * Constructor de BotonBoard
         * @param id valor
         * @param state true/false (Encendido/Apagado)
         */
        BotonBoard(Id id=NONE,bool state= false):value{id},on{state}{};
        /**
         * Destructor
         */
        ~BotonBoard(){};
        /**
         * Cambimos el estado
         * @param state true=encendido false=apagado
         */
        void Switch(bool state);
        /**
         * Devolvemos el estado
         * @return true=encendido false=apagado
         */
        bool getState(){return on;};
        /**
         * Establecemos posición
         * @param x Posición X
         * @param y Posición Y
         */
        void SetPosition(float x, float y);
        /**
         * Establecemos el tamaño
         * @param w Ancho
         * @param h Alto
         */
        void SetMeasure(float w, float h);//Establecemos el tamaño

    private:
        bool on;//Encendido?
    };
}