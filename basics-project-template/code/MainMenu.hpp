/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <basics/Scene>
#include <basics/Texture_2D>
#include <basics/Texture_2D>
#pragma once
#include "Simon.hpp"
#include "Info.hpp"
using
basics::Texture_2D;

namespace example
{


    /**
     * Clase MainMenu
     */
    class MainMenu : public basics::Scene
    {

        typedef std::shared_ptr< basics::Texture_2D > Texture_Handle;
        Boton playButton,info;//Botones que dispondremos en pantalla


        Texture_Handle bg; //Imagen que usaremos de fondo



    public:

        enum State
        {
            LOADING,
            RUNNING,
        };

        State          state;
        bool           suspended;

        unsigned       canvas_width;
        unsigned       canvas_height;


        float          x, y;

    public:

       /**
        *
        */
        MainMenu();

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        /**
         * Función que se ejecuta al inicar la clase
         * @return
         */
        bool initialize () override;
        /**
         * Función que se ejecuta cuando el juego está en suspensión
         */
        void suspend    () override;//Cuando el juego se pone en suspension
        /**
         * Función que se ejecuta cuando se reanuda el juego
         */
        void resume     () override;//Cuando el juego se renauda

        /**
         * Controla los eventos de pantalla
         *
         * @param event Evento de pantalla
         */
        void handle     (basics::Event & event) override; //controla los eventos de pantalla

        /**
         * En el se incluye la lógica del juego
         * @param time
         */
        void update     (float time) override;
        /**
         * Se encarga de mostrar por pantalla todos lo sprites y limpiar el canvas
         * @param context Contexto de la app
         */
        void render     (basics::Graphics_Context::Accessor & context) override;

    private:

        /**
         * Carga los datos (ene este caso es las texturas)
         */
        void load ();

        /**
         * se encarga de llamar a todas las funciones necesarias por frame
         * @param time
         */
        void run  (float time);
    };

}
