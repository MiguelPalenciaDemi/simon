/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Board.hpp"

#include <basics/Canvas>
#include <basics/Director>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>

using namespace example;

//Apaga/Muestra un boton
void Board::SwitchOne(size_t position, bool state)
{
    Buttons[position].Switch(state);//Cambiamos el estado del boton
}

//Apaga/Muestra todos boton
void Board::SwitchAll(bool state)
{
    //Cambiamos el estado de todos los botones
    for (size_t i= 0; i < 4; ++i) Buttons[i].Switch(state);
}

//Comprobamos el boton que hemos tocado (LE PASAMOS X/Y DEL PUNTO)
int Board::checkTouch(float x, float y)
{
    //Calculamos la distancia del punto al centro
    float _x=x-center.x;
    float x2= pow(_x,2.0);
    float _y=y-center.y;
    float y2= pow(_y,2.0);
    float module=sqrt(x2+y2);

    //Si la distancia es menor al radio externo y mayor que el interior quiere decir que hemos pinchado dentro.
    //Dependiendo ya de el signo de X/Y pinchamos en un boton u otro
    if(module<=external_radious && module>internal_radious)
    {
        if(x<center.x && y>center.y)return Buttons[0].value;
        else if(x>center.x && y>center.y)return Buttons[1].value;
        else if(x<center.x && y<center.y)return Buttons[2].value;
        else return Buttons[3].value;
    } else
        return BotonBoard::NONE;//Si no pinchamos en ninguno devolvemos NONE=4 y el SIMON no detectará ningun boton

}

//Muestra solo un boton.
void Board::ShowOne(size_t id)
{
    SwitchAll(false);
    SwitchOne(id,true);

}