/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <basics/Scene>
#include <basics/Texture_2D>
#include <basics/Texture_2D>
#pragma once
#include "Simon.hpp"
using
basics::Texture_2D;

namespace example
{


    class Info : public basics::Scene
    {



        typedef std::shared_ptr< basics::Texture_2D > Texture_Handle;

        /**
         * Puntero a la escena
         */
        std::shared_ptr<basics::Scene> goToScene;

    public:

        enum State
        {
            LOADING,
            RUNNING,
        };

        State          state;
        bool           suspended;
        /**
         * Sprites
         */
        Texture_Handle info1,info2;
        unsigned       canvas_width;
        unsigned       canvas_height;

        /**
         * Botones
         */
        Boton NextPage,PreviousPage,Back;
        /**
         * Booleano que usaré para saber que sprites pintar
         */
        bool nextPage;

        float          x, y;

    public:
        /**
         * Constructor Info Scene
         * @param lastScene Scene que queremos volver.
         */
        Info(std::shared_ptr<basics::Scene> lastScene);//Constructor. Le pasamos un puntero a una escena, para saber a que tipo de escena volver.

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        /**
         * Función que se ejecuta al inicar la clase
         * @return
         */
        bool initialize () override;
        /**
         * Función que se ejecuta cuando el juego está en suspensión
         */
        void suspend    () override;//Cuando el juego se pone en suspension
        /**
         * Función que se ejecuta cuando se reanuda el juego
         */
        void resume     () override;//Cuando el juego se renauda

        /**
         * Controla los eventos de pantalla
         *
         * @param event Evento de pantalla
         */
        void handle     (basics::Event & event) override; //controla los eventos de pantalla

        /**
         * En el se incluye la lógica del juego
         * @param time
         */
        void update     (float time) override;
        /**
         * Se encarga de mostrar por pantalla todos lo sprites y limpiar el canvas
         * @param context Contexto de la app
         */
        void render     (basics::Graphics_Context::Accessor & context) override;

    private:

        /**
         * Carga los datos (ene este caso es las texturas)
         */
        void load ();

        /**
         * se encarga de llamar a todas las funciones necesarias por frame
         * @param time
         */
        void run  (float time);
    };



}
