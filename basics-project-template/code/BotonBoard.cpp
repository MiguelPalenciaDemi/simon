/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "BotonBoard.hpp"
#include <basics/Canvas>
#include <basics/Director>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>
using namespace example;

//Establecemos las medidas
void BotonBoard::SetMeasure(float w, float h)
{
    width=w;
    height=h;
};


void BotonBoard::SetPosition(float x, float y) {this->x=x; this->y=y;};//Establecemos la posicion
void BotonBoard::Switch(bool state) {on=state;};//Establecemos el estado