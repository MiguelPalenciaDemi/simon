/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Simon.hpp"



//#include "Sample_Scene.hpp"
#include <basics/Canvas>
#include <basics/Scene>
#include <basics/Director>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>

#include<stdlib.h>
#include <time.h>


#include "Info.hpp"
using namespace basics;
using namespace std;

namespace example
{

    Simon::Simon()
    {
        //Inicializamos lo valores del canvas
        canvas_width  = 1280;
        canvas_height =  720;

        //Inicializamos/Construimos el tablero
        board = Board(canvas_height/4-20,canvas_height/2-20);

        //Inicializamos/Construimos los botones
        Win =  Boton(113,476,canvas_width/2,canvas_height/2+200);
        Lose =  Boton(113,476,canvas_width/2,canvas_height/2+200);
        Information =  Boton(113,476,canvas_width/2,canvas_height/2+20);
        Restart =  Boton(100,476,canvas_width/2,canvas_height/2-100);
        Exit =  Boton(100,476,canvas_width/2,canvas_height/2-210);
        Pause =  Boton(100,100,120,600);
        Easy =  Boton(100,476,canvas_width/2,canvas_height-200);
        Medium =   Boton(100,476,canvas_width/2,canvas_height/2);
        Hard =   Boton(100,476,canvas_width/2,200);

    }

    //Inicializamos el resto de variables
    bool Simon::initialize ()
    {
        srand(time(NULL));
        state     = LOADING;
        suspended = false;
        x         = 640;
        y         = 360;

        //Centro del tablero
        board.center.x=x;
        board.center.y=y;

        //Ajustamos los botones
        SetMeasureButtons();
        SetPositionButtons();

        turnoPos=0;
        turno=1;

        mode=Start;//Comenzamos con la seleccion de dificulatad

        return true;
    }

    void Simon::suspend ()
    {
        suspended = true;
    }

    void Simon::resume ()
    {
        suspended = false;
    }

    //Controlamos los eventos de teclado
    void Simon::handle (Event & event)
    {

        if (state == RUNNING)
        {
            switch (event.id)
            {

                //Cuando empieza el touch
                case ID(touch-started):
                {
                    //Guardamos el valor de donde hemos pulsado
                    x = *event[ID(x)].as< var::Float > ();
                    y = *event[ID(y)].as< var::Float > ();

                    //Si es el turno del jugador
                    if(mode==Player )
                    {
                        //Comprobamos si pulsa en alguno de los botones y si es así nos deberá devolver un numero entre 0 y 3
                        if(board.checkTouch(x,y) < 4)
                        {
                            //Guardamos el valor en el vector del jugador
                            player[turnoPos]= board.checkTouch(x,y);
                            //Avanzamos a la siguiente posicion
                            turnoPos++;


                        } else if(Pause.CheckPress(x,y))//Si pulsamos el botonn de pausa
                        {
                            mode=Paused;//Entramos en el modo Pause
                        }

                    }else if(mode==End || mode == Paused)//Si estamos en el modo pause o Fin del Juego
                    {
                        if(Pause.CheckPress(x,y) && mode==Paused)//Si tocamos el boton de Pausa otra vez, volvemos al juego
                            mode=Player;
                        if(Restart.CheckPress(x,y))//Si tocamos el botón Restart reiniciamos la partida
                        {
                           initialize();
                        } else if(Exit.CheckPress(x,y))//Si pulsamos el exit, volvemos al menu principal
                        {
                            director.run_scene (shared_ptr< Scene >(new MainMenu));
                        } else if(Information.CheckPress(x,y) && mode==Paused)//Si pulsamos el boton info vamos a la pantalla de Info
                            director.run_scene (shared_ptr< Scene >(new Info(shared_ptr< Scene >(new Simon))));

                    }else if(mode==Start)//Si estamos en el modo de seleccionar dificultad
                    {
                        if(Easy.CheckPress(x,y))
                        {
                            difficulty=EASY;
                            SelectDificulty();
                            StartSequence();

                        } else if(Hard.CheckPress(x,y))
                        {
                            difficulty=HARD;
                            SelectDificulty();
                            StartSequence();

                        }else if(Medium.CheckPress(x,y))
                        {
                            difficulty=MEDIUM;
                            SelectDificulty();
                            StartSequence();

                        }
                        if(Hard.CheckPress(x,y) || Easy.CheckPress(x,y)|| Medium.CheckPress(x,y))//Comenzamos la secuencia
                        {
                            mode=Sequence;
                            timer.reset();
                        }


                    }


                }
                case ID(touch-moved):
                case ID(touch-ended):
                {



                }
            }
        }
    }

    void Simon::update (float time)
    {
        switch (state)
        {
            case LOADING: load ();     break;
            case RUNNING: run  (time); break;
        }
    }

    void Simon::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended && state == RUNNING)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear        ();
                canvas->fill_rectangle ({ Pause.x,Pause.y}, { Pause.width,Pause.height }, Pause.image.get ());//Pintamos el boton de Pausa
                if(mode==Start)//En modo seleccionar dificultad pintaremos los botones que corresponden
                {
                    canvas->fill_rectangle ({ Easy.x,Easy.y}, { Easy.width,Easy.height }, Easy.image.get ());
                    canvas->fill_rectangle ({ Medium.x,Medium.y}, { Medium.width,Medium.height }, Medium.image.get ());
                    canvas->fill_rectangle ({ Hard.x,Hard.y}, { Hard.width,Hard.height }, Hard.image.get ());

                }else
                {
                    if(mode==Player || mode==Sequence)//Si es el modo de Secuencia o del jugador pintamos los botones del tablero  que esten encendidos

                            for(size_t i=0;i<4;++i)
                            {
                                if(board.Buttons[i].getState())
                                    canvas->fill_rectangle ({ board.Buttons[i].x,board.Buttons[i].y}, { board.Buttons[i].width,board.Buttons[i].height }, board.Buttons[i].image.get ());
                            }


                    if(mode==End || mode == Paused)//Si estamos en pausa o hemos terminado el juego
                    {
                        if(mode==End)//Si hemos terminado pintamos el letrero segun hayamos ganado o perdido
                        {
                            if(win)
                                canvas->fill_rectangle ({ Win.x,Win.y}, { Win.width,Win.height }, Win.image.get ());
                            else
                                canvas->fill_rectangle ({ Lose.x,Lose.y}, { Lose.width,Lose.height }, Lose.image.get ());



                        } else // si solo hemos dado a pausa vamos a pintar el boton de información
                        {
                            canvas->fill_rectangle ({ Information.x,Information.y}, { Information.width,Information.height }, Information.image.get ());
                        }
                        //Sin importar un estado u otro siempre aparecen los botones de restart y exit.
                        canvas->fill_rectangle ({ Restart.x,Restart.y}, { Restart.width,Restart.height }, Restart.image.get ());
                        canvas->fill_rectangle ({ Exit.x,Exit.y}, { Exit.width,Exit.height }, Exit.image.get ());



                    }
                }






            }
        }
    }

    //Cargamos todas las imagenes
    void Simon::load ()
    {
        if (!suspended)
        {
            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                Win.image = Texture_2D::create (ID(test), context, "Win.png");
                Lose.image = Texture_2D::create (ID(test), context, "Lose.png");
                Restart.image = Texture_2D::create (ID(test), context, "restart.png");
                Exit.image = Texture_2D::create (ID(test), context, "exit.png");
                Pause.image = Texture_2D::create (ID(test), context, "pause.png");
                Easy.image = Texture_2D::create (ID(test), context, "easy.png");
                Hard.image = Texture_2D::create (ID(test), context, "hard.png");
                Medium.image = Texture_2D::create (ID(test), context, "medium.png");
                Information.image = Texture_2D::create (ID(test), context, "info.png");
                board.Buttons[0].image = Texture_2D::create (ID(test), context, "verde.png");
                board.Buttons[1].image = Texture_2D::create (ID(test), context, "rojo.png");
                board.Buttons[2].image = Texture_2D::create (ID(test), context, "amarillo.png");
                board.Buttons[3].image = Texture_2D::create (ID(test), context, "azul.png");


                if (board.Buttons[3].image)
                {
                    context->add (Win.image);
                    context->add (Lose.image);
                    context->add (Restart.image);
                    context->add (Exit.image);
                    context->add (Pause.image);
                    context->add (Easy.image);
                    context->add (Medium.image);
                    context->add (Hard.image);
                    context->add (Information.image);
                    context->add (board.Buttons[0].image);
                    context->add (board.Buttons[1].image);
                    context->add (board.Buttons[2].image);
                    context->add (board.Buttons[3].image);

                    state = RUNNING;
                }
            }
        }
    }

    //Lleva la logica del juego
    void Simon::run (float )
    {

         if(mode==Sequence)//Si estamos en secuencia
        {
            if (turnoPos<turno)//Si la posicion es menor al turno
            {
                board.ShowOne(sequence[turnoPos]);//Mostramos solo el boton que indica el vector de la secuencia
                elapsedTime = timer.get_elapsed_seconds();//guardamos el tiempo que ha trascurrido desde que iniciamos el timer
                if(elapsedTime>=interval)//Si ha superado el tiempo estipulado
                {
                    board.SwitchAll(false);//Apagamos todos los botones
                    if(elapsedTime>=interval+0.25)//Esperamos un poco más
                    {
                        timer.reset();//reiniciamos el time
                        turnoPos++;//Avanzamos una posición
                    }



                }
            }
            else//Si hemos llegado al numero de turno correspondiente
            {
                board.SwitchAll(true);//Encendemos todos los botones
                mode=Player;//pasamos al modo de juego
                turnoPos=0;//reiniciamos la posicion
            }




        } else //si es otro turno (PLAYER)
        {
            if(turnoPos>=turno)//Comprobamos que no nos hayamos pasado. Si es así....
            {
                if(!CheckCorrect())//Comprobamos si los vectores coinciden.Si es negativo perdemos
                    GameOver(false);
                else//Si es correcto
                {
                    turnoPos=0;//reiniciamos la posicion
                    turno++;//aumentamos un turno
                    if(turno==totalTurnos)//si es el ultio turno significa que hemos ganado
                        GameOver(true);
                    else //si no es el final
                        mode=Sequence;//Cambiamos el estado de juego a la secuencia y reiniciamos el timer
                    timer.reset();
                };

            }
        }

    }

    //Damos tamaño a todos los botones.
    void Simon::SetMeasureButtons()
    {
        for(size_t i=0;i<4;++i)
            board.Buttons[i].SetMeasure(board.external_radious,board.external_radious);

    }

    //Damos posicion a los botones
    void Simon::SetPositionButtons()
    {
       // for(size_t i=0;i<4;++i) board.Buttons[i].SetPosition(board.center.x-board.Buttons[i].width/2,board.center.y+board.Buttons[i].height/2);
        board.Buttons[0].SetPosition(board.center.x-board.Buttons[0].width/2,board.center.y+board.Buttons[0].height/2);
        board.Buttons[1].SetPosition(board.center.x+board.Buttons[1].width/2,board.center.y+board.Buttons[1].height/2);
        board.Buttons[2].SetPosition(board.center.x-board.Buttons[2].width/2,board.center.y-board.Buttons[2].height/2);
        board.Buttons[3].SetPosition(board.center.x+board.Buttons[3].width/2,board.center.y-board.Buttons[3].height/2);
    }

    //Rellenamos el vector de la secuencia hasta con numero aleatorios hasta de 0 a 3
    void Simon::StartSequence()
    {
        int tmp;
        for(size_t i = 0; i<totalTurnos;++i)
        {
           tmp=rand()%(4);
           sequence[i]=tmp;
        }
    }

    //Comprobamos si coinciden los vectores
    bool Simon:: CheckCorrect()
    {
        size_t count=0;

        for( ; count <turno;++count)
        {

            if(player[count]!= sequence[count])
            {
                return false;
            }
        };

        return true;
    }

    //Juego termina
    void Simon::GameOver(bool state)
    {
        mode=End;//cambiamos a Fin del Juego
        board.SwitchAll(false);//Apagamos todos los botones
        win=state;//damos valor a la variable
    }

    //Seleciona la dificultad y da valores dependiendo del valor del estado
    void Simon::SelectDificulty()
    {
        if(difficulty==HARD)
        {
           totalTurnos=20;
            interval = 0.5;

        } else if(difficulty==MEDIUM)
        {
            totalTurnos = 15;
            interval = 1;

        } else
        {
            totalTurnos=10;
            interval = 1.5;
        }

        player.resize(totalTurnos);//damos el valor adecuado al vector del jugador
        sequence.resize(totalTurnos);//damos el valor adecuado al vector de la secuencia

    }

}
