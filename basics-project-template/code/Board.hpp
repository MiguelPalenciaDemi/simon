/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <basics/Scene>
#include <basics/Texture_2D>
#include <basics/Texture_2D>
#include "BotonBoard.hpp"

using basics::Texture_2D;

namespace example
{

#pragma once
    class Board
    {
        /**
         * Centro de tablero Estructura
         */
        struct Center
        {
            float x,y;
        };
        typedef std::shared_ptr< basics::Texture_2D > Texture_Handle;
    public:

        /**
         * Botones del tablero
         */
        BotonBoard Buttons[4]{{BotonBoard::A, false}, {BotonBoard::B, false},{BotonBoard::C, false},{BotonBoard::D, false}};
        /**
         * Radios del tablero
         */
        float internal_radious,external_radious;
        /**
         * Centro del tablero
         */
        Center center;

        /**
         * Constructor de Board
         * @param r_internal  radio externo
         * @param r_external  radio interior
         */
        Board(float r_internal = 0, float r_external=0):internal_radious{r_internal},external_radious{r_external}{};
        /**
         * Destructor
         */
        ~Board(){}

        /**
         * Apaga/Muestra un boton
         */

        void SwitchOne(size_t position, bool state);
        /**
         * Apaga/Muestra todos los botones
         */
        void SwitchAll(bool state);
        /**
         * Muestra un boton
         */
        void ShowOne(size_t id);
        /**
         * Comprueba que botón hemos tocado
         */
        int checkTouch(float x=0,float y=0);
    };
}