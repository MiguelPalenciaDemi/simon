/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Boton.hpp"
#include <basics/Canvas>
#include <basics/Director>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>

namespace example
{
    //Comprobamos si la X y la Y está dentro del boton
    bool Boton::CheckPress(float _x, float _y)
    {
        return(_x<x+width/2 && _x>x-width/2 && _y<y+height/2 && _y>y-height/2);
    }

}