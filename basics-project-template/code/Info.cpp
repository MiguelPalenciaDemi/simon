/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Info.hpp"


#include <basics/Canvas>
#include <basics/Director>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>

using namespace basics;
using namespace std;

namespace example
{

    //Inicializamos las variables
    Info::Info(shared_ptr< Scene > lastScene)
    {
        nextPage = false;
        goToScene=lastScene;//Asignamos la escena a la que volveremos
        canvas_width  = 1280;
        canvas_height =  720;
        Back = Boton(100,100,120,600);
        PreviousPage = Boton(100,100,100,canvas_height/2);
        NextPage = Boton(100,100,canvas_width-110,canvas_height/2);
    }

    bool Info::initialize ()
    {
        state     = LOADING;
        suspended = false;
        x         = 640;
        y         = 360;

        return true;
    }

    void Info::suspend ()
    {
        suspended = true;
    }

    void Info::resume ()
    {
        suspended = false;
    }

    void Info::handle (Event & event)
    {
        if (state == RUNNING)
        {

            switch (event.id)
            {
                case ID(touch-started):
                {
                    x = *event[ID(x)].as< var::Float > ();
                    y = *event[ID(y)].as< var::Float > ();

                    if(Back.CheckPress(x,y))//Si pulso back vuelvo al tipo de escena del que volvia
                    {
                        director.run_scene (goToScene);
                    }
                    else if(NextPage.CheckPress(x,y) && !nextPage)//Pasamos de "página"
                        nextPage = true;
                    else if(PreviousPage.CheckPress(x,y) && nextPage)
                        nextPage = false;
                    break;
                }
                case ID(touch-moved):
                case ID(touch-ended):
                {


                }
            }
        }
    }

    void Info::update (float time)
    {
        switch (state)
        {
            case LOADING: load ();     break;
            case RUNNING: run  (time); break;
        }
    }

    void Info::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended && state == RUNNING)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear        ();

                //Dependiendo de la "página" mostraremos un sprite u otro
                canvas->fill_rectangle ({ Back.x,Back.y}, { Back.width,Back.height }, Back.image.get ());
                if(!nextPage)
                {
                    canvas->fill_rectangle ({ NextPage.x,NextPage.y}, { NextPage.width,NextPage.height }, NextPage.image.get ());
                    canvas->fill_rectangle ({ 640,360}, {1280,720 }, info1.get ());
                }

                else
                {
                    canvas->fill_rectangle ({ PreviousPage.x,PreviousPage.y}, { PreviousPage.width,PreviousPage.height }, PreviousPage.image.get ());
                    canvas->fill_rectangle ({ 640,360}, { 1280,720 }, info2.get ());
                }


            }
        }
    }

    //Cargamos las imagenes
    void Info::load ()
    {
        if (!suspended)
        {
            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                info1 = Texture_2D::create (ID(test), context, "info1.png");
                info2 = Texture_2D::create (ID(test), context, "info2.png");
                Back.image = Texture_2D::create (ID(test), context, "back.png");
                NextPage.image = Texture_2D::create (ID(test), context, "next.png");
                PreviousPage.image = Texture_2D::create (ID(test), context, "previous.png");


                if (PreviousPage.image)
                {

                    context->add (info1);
                    context->add (info2);
                    context->add (NextPage.image);
                    context->add (PreviousPage.image);
                    context->add (Back.image);

                    state = RUNNING;
                }
            }
        }
    }

    void Info::run (float )
    {


    }

}
