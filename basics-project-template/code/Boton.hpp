/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <basics/Scene>
#include <basics/Texture_2D>
#include <basics/Texture_2D>

using basics::Texture_2D;

namespace example
{

    /**
     * Clase Boton
     */
    class Boton
    {
        typedef std::shared_ptr< basics::Texture_2D > Texture_Handle;
    public:
        /**
         * Sprite
         */
        Texture_Handle image;
        /**
         * Dimensiones
         */
        float width,height;
        /**
         *Posicion
         */
        float x,y;
        /**
         * Constructor
         * @param h Altura
         * @param w Ancho
         * @param x posición X
         * @param y posición Y
         */
        Boton(float h=0, float w=0, float x=0,float y=0):height{h},width{w},x{x},y{y}{};
        /**
         * Destructor
         */
        ~Boton(){};

        /**
         * Comprobamos si hemos pulsado el boton
         */
        bool CheckPress(float _x, float _y);
    };
}