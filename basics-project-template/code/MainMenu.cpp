/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "MainMenu.hpp"


#include <basics/Canvas>
#include <basics/Director>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>

using namespace basics;
using namespace std;

namespace example
{

     MainMenu::MainMenu()
    {

        //inicializamos el valor de las medidas del canvas
        canvas_width  = 1280;
        canvas_height =  720;

        //Contruimos/Inicializamos los botones.
        playButton=Boton(97,480,canvas_width/2,220);
        info=Boton(97,480,canvas_width/2,100);
    }

    //Inicializamos el resto de variables
    bool MainMenu::initialize ()
    {
        state     = LOADING;
        suspended = false;
        x         = 640;
        y         = 360;

        return true;
    }

    void MainMenu::suspend ()
    {
        suspended = true;
    }

    void MainMenu::resume ()
    {
        suspended = false;
    }

    //Controlamos los eventos de pantalla
    void MainMenu::handle (Event & event)
    {
        //Si estamos el juego corriendo
        if (state == RUNNING)
        {

            switch (event.id)
            {
                //Cuando empieza el touch
                case ID(touch-started):
                {
                    //guardamos el valor del punto donde tocamos en pantalla
                    x = *event[ID(x)].as< var::Float > ();
                    y = *event[ID(y)].as< var::Float > ();

                    //Comprobamos si tocamos en los botones.
                    if(playButton.CheckPress(x,y))
                    {
                        director.run_scene (shared_ptr< Scene >(new Simon));//Iniciamos el Simon
                    } else if(info.CheckPress(x,y))
                        director.run_scene (shared_ptr< Scene >(new Info{shared_ptr< Scene >(new MainMenu)}));//Iniciamos la pantalla de Info pasando un puntero con la clase que queremos volver.
                    break;
                }
                case ID(touch-moved):
                case ID(touch-ended):
                {


                }
            }
        }
    }

    void MainMenu::update (float time)
    {
        switch (state)
        {
            case LOADING: load ();     break;
            case RUNNING: run  (time); break;
        }
    }

    void MainMenu::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended && state == RUNNING)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                //Creamos el canvas si aun no existe
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                //Refrescamos la pantalla
                canvas->clear        ();
                //Pintamos los sprites
                canvas->fill_rectangle ({ 640,360}, { 1280,720 }, bg.get ());
                canvas->fill_rectangle ({ playButton.x,playButton.y}, { playButton.width,playButton.height }, playButton.image.get ());
                canvas->fill_rectangle ({ info.x,info.y}, { info.width,info.height }, info.image.get ());

            }
        }
    }

    //Cargamos la imagen
    void MainMenu::load ()
    {
        if (!suspended)
        {
            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                playButton.image = Texture_2D::create (ID(test), context, "simonButton.png");
                info.image = Texture_2D::create (ID(test), context, "info.png");
                bg = Texture_2D::create (ID(test), context, "Simon.png");


                if (playButton.image)
                {
                    context->add (playButton.image);
                    context->add (info.image);
                    context->add (bg);

                    state = RUNNING;
                }
            }
        }
    }

    void MainMenu::run (float )
    {


    }

}
